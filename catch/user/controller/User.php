<?php

namespace catchAdmin\user\controller;

use catcher\base\CatchRequest as Request;
use catcher\CatchResponse;
use catcher\base\CatchController;
use catchAdmin\user\model\User as UserModel;
use think\Response;

class User extends CatchController
{

    protected $userModel;

    /**
     *
     * @time 2021/11/12 11:52
     * @param UserModel $userModel
     * @return mixed
     */
    public function __construct(UserModel $userModel)
    {
        $this->userModel = $userModel;
    }

    /**
     *
     * @time 2021/11/12 11:52
     * @return Response
     */
    public function index(): Response
    {
        return CatchResponse::paginate($this->userModel->getList());
    }

    /**
     *
     * @time 2021/11/12 11:52
     * @param Request $request
     * @return Response
     */
    public function save(Request $request): Response
    {
        return CatchResponse::success($this->userModel->storeBy($request->post()));
    }

    /**
     *
     * @time 2021/11/12 11:52
     * @param $id
     * @return Response
     */
    public function read($id): Response
    {
        return CatchResponse::success($this->userModel->findBy($id));
    }

    /**
     *
     * @time 2021/11/12 11:52
     * @param $id
     * @param Request $request
     * @return Response
     */
    public function update($id, Request $request): Response
    {
        return CatchResponse::success($this->userModel->updateBy($id, $request->post()));
    }

    /**
     *
     * @time 2021/11/12 11:52
     * @param $id
     * @return Response
     */
    public function delete($id): Response
    {
        return CatchResponse::success($this->userModel->deleteBy($id));
    }
}
