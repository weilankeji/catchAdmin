<?php

namespace catchAdmin\user\tables;

use catcher\CatchTable;
use catcher\library\table\HeaderItem;
use catcher\library\table\Search;

class User extends CatchTable
{
    public function table()
    {
        // TODO: Implement table() method.
        return $this->getTable('user')
            ->header([
                HeaderItem::label('')->selection(),
                HeaderItem::label('ID')->prop('id')->width(80),
                HeaderItem::label('昵称')->prop('nickname'),
                HeaderItem::label('头像')->prop('avatar_url')->withPreviewComponent(),
                HeaderItem::label('电话')->prop('phone'),
                HeaderItem::label('创建时间')->prop('created_at'),
            ])
            ->withBind()
            ->withSearch([
                Search::label('昵称')->text('nickname', '请输入昵称'),
                Search::label('电话')->text('phone', '请输入电话'),
            ])
            ->withApiRoute('user')
            ->selectionChange()
            ->render();
    }

    protected function form()
    {
        // TODO: Implement form() method.
    }
}
