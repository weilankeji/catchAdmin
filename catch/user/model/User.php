<?php

namespace catchAdmin\user\model;

use catchAdmin\user\model\search\UserSearch;
use catcher\base\CatchModel as Model;

/**
 *
 * @property int $id
 * @property string $nickname
 * @property string $avatar_url
 * @property string $openid
 * @property string $phone
 * @property datetime $last_time
 * @property int $created_at
 * @property int $updated_at
 * @property int $deleted_at
 */
class User extends Model
{
    use UserSearch;

    public $field = [
        //
        'id',
        // 昵称
        'nickname',
        // 头像
        'avatar_url',
        // 微信用户的唯一标识
        'openid',
        // 电话
        'phone',
        // 最近一次登录时间
        'last_time',
        // 创建时间
        'created_at',
        // 更新时间
        'updated_at',
        // 软删除字段
        'deleted_at',
    ];

    public $name = 'user';
}
