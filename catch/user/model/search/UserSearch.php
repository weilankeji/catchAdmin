<?php

namespace catchAdmin\user\model\search;

trait UserSearch
{
    public function searchNicknameAttr($query, $value, $data)
    {
        return $query->whereLike('nickname', $value);
    }

    public function searchPhoneAttr($query, $value, $data)
    {
        return $query->whereLike('phone', $value);
    }
}
