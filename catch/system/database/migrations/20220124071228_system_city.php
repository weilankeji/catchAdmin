<?php
// +----------------------------------------------------------------------
// | CatchAdmin [Just Like ～ ]
// +----------------------------------------------------------------------
// | Copyright (c) 2017~{$year} http://catchadmin.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( https://github.com/yanwenwu/catch-admin/blob/master/LICENSE.txt )
// +----------------------------------------------------------------------
// | Author: JaguarJack [ njphper@gmail.com ]
// +----------------------------------------------------------------------

use Phinx\Db\Adapter\MysqlAdapter;
use think\migration\Migrator;

class SystemCity extends Migrator
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('system_city', ['engine' => 'InnoDB', 'collation' => 'utf8mb4_general_ci', 'comment' => '全国省市区表', 'id' => 'id', 'primary_key' => ['id']]);
        $table->addColumn('pid', 'integer', ['limit' => MysqlAdapter::INT_REGULAR, 'null' => false, 'default' => 0, 'signed' => false, 'comment' => '父id',])
            ->addColumn('shortname', 'string', ['limit' => 100, 'null' => false, 'default' => '', 'signed' => true, 'comment' => '简称',])
            ->addColumn('name', 'string', ['limit' => 100, 'null' => false, 'default' => '', 'signed' => true, 'comment' => '名称',])
            ->addColumn('merger_name', 'string', ['limit' => 255, 'null' => false, 'default' => '', 'signed' => true, 'comment' => '全称',])
            ->addColumn('level', 'integer', ['limit' => MysqlAdapter::INT_TINY, 'null' => false, 'default' => 0, 'signed' => false, 'comment' => '层级 1 2 3 省市区县',])
            ->addColumn('pinyin', 'string', ['limit' => 100, 'null' => false, 'default' => '', 'signed' => true, 'comment' => '拼音',])
            ->addColumn('code', 'string', ['limit' => 100, 'null' => false, 'default' => '', 'signed' => true, 'comment' => '长途区号',])
            ->addColumn('zip_code', 'string', ['limit' => 100, 'null' => false, 'default' => '', 'signed' => true, 'comment' => '邮编',])
            ->addColumn('first', 'string', ['limit' => 50, 'null' => false, 'default' => '', 'signed' => true, 'comment' => '首字母',])
            ->addColumn('lng', 'string', ['limit' => 100, 'null' => false, 'default' => '', 'signed' => true, 'comment' => '经度',])
            ->addColumn('lat', 'string', ['limit' => 100, 'null' => false, 'default' => '', 'signed' => true, 'comment' => '纬度',])
            ->create();
    }
}
