<?php

namespace catchAdmin\system\model;

use think\Model;
use catcher\traits\db\BaseOptionsTrait;
use catcher\traits\db\ScopeTrait;
/**
 *
 * @property int $id
 * @property int $pid
 * @property string $shortname
 * @property string $name
 * @property string $merger_name
 * @property int $level
 * @property string $pinyin
 * @property string $code
 * @property string $zip_code
 * @property string $first
 * @property string $lng
 * @property string $lat
 */
class SystemCity extends Model
{
    use BaseOptionsTrait, ScopeTrait;

    public $field = [
        //
        'id',
        // 父id
        'pid',
        // 简称
        'shortname',
        // 名称
        'name',
        // 全称
        'merger_name',
        // 层级 1 2 3 省市区县
        'level',
        // 拼音
        'pinyin',
        // 长途区号
        'code',
        // 邮编
        'zip_code',
        // 首字母
        'first',
        // 经度
        'lng',
        // 纬度
        'lat',
    ];

    public $name = 'system_city';
}