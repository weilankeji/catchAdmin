<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2018 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------

use think\facade\Route;
use catcher\CatchResponse;
use catchAdmin\permissions\middleware\AuthTokenMiddleware;

Route::group('api/', function () {
    Route::any('test', 'api.Auth/test');

    // 无需token验证
    Route::group(function () {
        // 小程序通用
        Route::group(function () {
            // 获取登录凭证
            Route::get('wxLogin/<code>', '/wxLogin');
            // 获取微信用户信息
            Route::post('wxGetUserInfo', '/wxGetUserInfo');
            // 获取微信用户绑定的手机号
            Route::post('wxGetPhoneNumber', '/wxGetPhoneNumber');
        })->prefix('api.Auth');
    });

    // 强制token验证
    Route::group(function () {

    })->middleware(AuthTokenMiddleware::class);

    Route::miss(function () {
        return CatchResponse::fail('地址异常：找不到该路由');
    });
});
