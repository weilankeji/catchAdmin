<?php

return [
    // 小程序
    'mini_program' => [
        'app_id' => env('MINIPROGRAM.appid'),
        'secret' => env('MINIPROGRAM.secret'),

        // 下面为可选项
        // 指定 API 调用返回结果的类型：array(default)/collection/object/raw/自定义类名
        'response_type' => 'array',

        'log' => [
            'level' => 'debug',
            'file' => __DIR__ . '/../runtime/log/' . date('Ym') . '/wechat.log',
        ],
    ],

    // 支付
    //'payment'          => [
    //    'default' => [
    //        'sandbox'    => false,
    //        'app_id'     => '',
    //        'mch_id'     => 'your-mch-id',
    //        'key'        => 'key-for-signature',
    //        'cert_path'  => 'path/to/cert/apiclient_cert.pem',    // XXX: 绝对路径！！！！
    //        'key_path'   => 'path/to/cert/apiclient_key.pem',      // XXX: 绝对路径！！！！
    //        'notify_url' => 'http://example.com/payments/wechat-notify',                           // 默认支付结果通知地址
    //    ],
    //    // ...
    //],
];
