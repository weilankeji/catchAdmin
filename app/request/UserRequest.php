<?php

namespace app\request;

use catcher\base\CatchRequest;

class UserRequest extends CatchRequest
{
    // 是否添加 creator_id
    protected $needCreatorId = false;

    protected function rules(): array
    {
        // TODO: Implement rules() method.
        return [
            'openid|微信用户的openid' => 'require|max:28',
            'iv|加密算法的初始向量' => 'require',
            'encryptedData|包括敏感数据在内的完整用户信息的加密数据' => 'require',
        ];
    }

    protected function message()
    {
        // TODO: Implement message() method.
    }
}
