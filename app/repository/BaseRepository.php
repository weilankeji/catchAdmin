<?php

namespace app\repository;

class BaseRepository
{
    /**
     * @var $dao
     */
    protected $dao;

    public function setDao($dao)
    {
        $this->dao = $dao;
    }

    public function __call($name, $arguments)
    {
        return call_user_func_array([$this->dao, $name], $arguments);
    }
}
